﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="WebApplication1.SignIn" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Cadastre-se</title>


</head>

<body>

    <h1>Cadastre-se</h1>
    <br />

    <form id="form1" runat="server" >
        <div>
            <asp:ScriptManager runat="server"></asp:ScriptManager>
            Usuario: 
            <br />
            <asp:TextBox runat="server" id="UsernameBox"></asp:TextBox>
            <br />

            Cpf: 
            <br />
            <asp:TextBox runat="server" id="CpfBox" MaxLength="11"></asp:TextBox>          
            <br />                        

            <ajaxToolkit:MaskedEditExtender
                Id="extender"
                runat="server"
                TargetControlID="CpfBox" 
                Mask="999\.999\.999\-99"
                MessageValidatorTip="true" 
                OnFocusCssClass="MaskedEditFocus"
                ClearMaskOnLostFocus="false"
                MaskType="Number" 
                InputDirection="RightToLeft" 
                ErrorTooltipEnabled="false"/>

            Company
            <br />
            <asp:DropDownList runat="server" id="CompanyDD">
                <asp:ListItem Text="Riot" />
                <asp:ListItem Text="Ubisoft" />
                <asp:ListItem Text="Gameloft" />
            </asp:DropDownList>
            <br />

            <asp:Button runat="server" Text="Cadastrar" OnClick="SignIn_Click"/>

        </div>

        <h1>Cadastrados</h1>

        <asp:Panel runat="server">
            <asp:GridView ID="ResultGrid" runat="server">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="DeleteButton" Text="Delete" runat="server" OnClick="Delete_Click" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>    
            </asp:GridView>

        </asp:Panel>
        <br />

    </form>
</body>
</html>
