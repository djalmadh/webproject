﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Scripts;

namespace WebApplication1
{
    public partial class SignIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void SignIn_Click(object sender, EventArgs e)
        {
            AddNewUser();
            UpdateDivHtml();
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            RemoveAtRow(sender);
            UpdateDivHtml();
        }

        public void AddNewUser()
        {
            if(UsernameBox.Text != "" && CpfBox.Text != "")
            UserHandler.AddNewUser(new User(UsernameBox.Text, CpfBox.Text, CompanyDD.SelectedValue));
        }

        public void UpdateDivHtml()
        {
            ResultGrid.DataSource = UserHandler.Users;
            ResultGrid.DataBind();
        }

        public void RemoveAtRow(object sender)
        {
            Button button = (Button)sender;
            GridViewRow gridViewRow = (GridViewRow)button.NamingContainer;
            UserHandler.RemoveUserAt(gridViewRow.RowIndex);
        }
    }
}