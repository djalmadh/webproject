﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Scripts
{
    public static class UserHandler
    {
        public static List<User> Users = new List<User>();

        public static void AddNewUser(User user)
        {
            Users.Add(user);
        }

        public static void RemoveUserAt(int index)
        {
            Users.RemoveAt(index);
        }
    }
}