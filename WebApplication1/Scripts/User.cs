﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Scripts
{
    public class User
    {
        public string name { get; set; }
        public string cpf { get; set; }
        public string company { get; set; }
        public User(string name, string cpf, string company)
        {
            this.name = name;
            this.cpf = cpf;
            this.company = company;
        }
    }
}